﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class chatwin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.usr_name = New System.Windows.Forms.Label()
        Me.foto = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.amigos = New System.Windows.Forms.ListBox()
        Me.msjbox = New System.Windows.Forms.TextBox()
        Me.chatbox_r = New System.Windows.Forms.RichTextBox()
        Me.timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.enviar = New System.Windows.Forms.PictureBox()
        Me.bwchat = New System.ComponentModel.BackgroundWorker()
        Me.chatbox = New System.Windows.Forms.RichTextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.foto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.enviar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Panel1.Controls.Add(Me.usr_name)
        Me.Panel1.Controls.Add(Me.foto)
        Me.Panel1.Location = New System.Drawing.Point(593, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(288, 113)
        Me.Panel1.TabIndex = 0
        '
        'usr_name
        '
        Me.usr_name.AutoSize = True
        Me.usr_name.Font = New System.Drawing.Font("Franklin Gothic Demi", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.usr_name.Location = New System.Drawing.Point(120, 9)
        Me.usr_name.Name = "usr_name"
        Me.usr_name.Size = New System.Drawing.Size(85, 24)
        Me.usr_name.TabIndex = 1
        Me.usr_name.Text = "NOMBRE"
        '
        'foto
        '
        Me.foto.Location = New System.Drawing.Point(18, 9)
        Me.foto.Name = "foto"
        Me.foto.Size = New System.Drawing.Size(96, 90)
        Me.foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.foto.TabIndex = 0
        Me.foto.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.RoyalBlue
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.amigos)
        Me.Panel2.Location = New System.Drawing.Point(593, 114)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(288, 516)
        Me.Panel2.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Demi", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(160, 24)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "LISTA DE AMIGOS"
        '
        'amigos
        '
        Me.amigos.BackColor = System.Drawing.Color.DodgerBlue
        Me.amigos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.amigos.FormattingEnabled = True
        Me.amigos.Location = New System.Drawing.Point(12, 44)
        Me.amigos.Name = "amigos"
        Me.amigos.Size = New System.Drawing.Size(266, 364)
        Me.amigos.TabIndex = 0
        '
        'msjbox
        '
        Me.msjbox.BackColor = System.Drawing.Color.White
        Me.msjbox.Location = New System.Drawing.Point(22, 495)
        Me.msjbox.Multiline = True
        Me.msjbox.Name = "msjbox"
        Me.msjbox.Size = New System.Drawing.Size(498, 44)
        Me.msjbox.TabIndex = 3
        '
        'chatbox_r
        '
        Me.chatbox_r.Location = New System.Drawing.Point(203, 39)
        Me.chatbox_r.Name = "chatbox_r"
        Me.chatbox_r.Size = New System.Drawing.Size(140, 61)
        Me.chatbox_r.TabIndex = 2
        Me.chatbox_r.Text = ""
        '
        'timer1
        '
        Me.timer1.Enabled = True
        Me.timer1.Interval = 1000
        '
        'enviar
        '
        Me.enviar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.enviar.Image = Global.airmail.My.Resources.Resources.send2
        Me.enviar.Location = New System.Drawing.Point(520, 495)
        Me.enviar.Name = "enviar"
        Me.enviar.Size = New System.Drawing.Size(51, 44)
        Me.enviar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.enviar.TabIndex = 4
        Me.enviar.TabStop = False
        Me.ToolTip1.SetToolTip(Me.enviar, "Send")
        '
        'bwchat
        '
        Me.bwchat.WorkerReportsProgress = True
        Me.bwchat.WorkerSupportsCancellation = True
        '
        'chatbox
        '
        Me.chatbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.chatbox.Location = New System.Drawing.Point(22, 15)
        Me.chatbox.Name = "chatbox"
        Me.chatbox.Size = New System.Drawing.Size(549, 411)
        Me.chatbox.TabIndex = 6
        Me.chatbox.Text = ""
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.airmail.My.Resources.Resources.link
        Me.PictureBox1.Location = New System.Drawing.Point(86, 435)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(58, 57)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.airmail.My.Resources.Resources.emoji
        Me.PictureBox3.Location = New System.Drawing.Point(22, 435)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(58, 57)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 5
        Me.PictureBox3.TabStop = False
        '
        'chatwin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 555)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.chatbox)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.enviar)
        Me.Controls.Add(Me.msjbox)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.chatbox_r)
        Me.Name = "chatwin"
        Me.Text = "chatwin"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.foto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.enviar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents usr_name As Label
    Friend WithEvents foto As PictureBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents amigos As ListBox
    Friend WithEvents enviar As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents chatbox_r As RichTextBox
    Friend WithEvents timer1 As Timer
    Friend WithEvents Label3 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents bwchat As System.ComponentModel.BackgroundWorker
    Friend WithEvents chatbox As RichTextBox
    Public WithEvents msjbox As TextBox
    Friend WithEvents PictureBox1 As PictureBox
End Class
