﻿Imports MySql.Data.MySqlClient
Public Class fotoperfil
    Private Sub fotoperfil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim mems As System.IO.MemoryStream = New System.IO.MemoryStream(Convert.FromBase64String(My.Settings.displaypic))
        PictureBox1.Image = Drawing.Image.FromStream(mems)
    End Sub

    Private Sub PictureBox2_Click() Handles PictureBox2.Click
        If cimg.ShowDialog = vbOK Then
            Try
                My.Settings.displaypic = imgtobytes(cimg.FileName)
                My.Settings.Save()

                Dim rpic As New MySqlCommand("UPDATE tb_users SET dp='" & My.Settings.displaypic & "' WHERE username='" & My.Settings.username & "'", connection)
                rpic.ExecuteNonQuery()
                MsgBox("Foto de perfil ha sido actualizada, Los cambios tendran efecto cuando cierre y vuela a iniciar sesion...", MsgBoxStyle.Information, "Operacion con exito!")

            Catch ex As Exception
                If ex.Message.Contains("Muchos paquetes") Then
                    MsgBox("La imagen que quiere subir es demasiado pesada para almacenarla, porfavor elija otra imagen del tamaño de: 300 x 300 px.", MsgBoxStyle.Critical, "Tamaño de imagen invalida!")
                Else
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "Error de dato")
                End If

            End Try
        End If
    End Sub
End Class