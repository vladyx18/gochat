﻿Imports MySql.Data.MySqlClient
Imports System.Security.Cryptography
Public Class main
    Dim accion As Boolean = False
    Private Sub main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        openconnection()
        usuario.Text = ""
        pass.Text = ""
        pass.PasswordChar = "*"
        Lblestado.Visible = False
    End Sub
    Private Sub main_formclosing() Handles MyBase.FormClosing
        connection.Close()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        connection.Close()
        Dispose()
    End Sub
    Private Sub PictureBox3_Click() Handles PictureBox3.Click
        accion = True
        If accion = True Then
            Dim comando As New MySqlCommand("SELECT * FROM tb_users", connection)
            Dim datareader As MySqlDataReader
            Try
                datareader = comando.ExecuteReader()
                While datareader.Read()
                    If datareader(1).ToString = usuario.Text Then
                        Lblestado.Visible = True
                        Lblestado.Text = "Usuario ya existente!"
                        Lblestado.ForeColor = Color.Black
                        datareader.Close()
                        Exit Sub
                    End If
                End While
                datareader.Close()
            Catch
                Lblestado.Visible = True
                Lblestado.ForeColor = Color.Black
                Lblestado.Text = "Error de BD!"
            End Try
            If Not IO.File.Exists(Application.StartupPath & "\users.png") Then
                MsgBox("El nombre del archivo users.png no se encuentra, porfavor reinstalar aplicacion", MsgBoxStyle.Critical, "IO ERROR")
                Exit Sub
            End If
            Dim inscom As New MySqlCommand("INSERT INTO tb_users (username, password, name, dp) VALUES('" & usuario.Text & "','" & md5encrypter(pass.Text) & "','" & usuario.Text & "','" & imgtobytes(Application.StartupPath & "\users.png") & "')", connection)
            Try
                inscom.ExecuteNonQuery()
                Lblestado.Visible = True
                Lblestado.Text = "Cuenta creada!"
                Lblestado.ForeColor = Color.Green
            Catch
                Lblestado.Visible = True
                Lblestado.ForeColor = Color.Black
                Lblestado.Text = "Error al crear cuenta!"
            End Try
        End If
    End Sub
    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        If usuario.Text = "username" Or usuario.Text = String.Empty Then
            Lblestado.Visible = True
            Lblestado.Text = "Usuario Invalido!"
            Lblestado.ForeColor = Color.Black

            Exit Sub
        End If
        If pass.Text = "password" Or pass.Text = String.Empty Then
            Lblestado.Visible = True
            Lblestado.Text = "Contraseña Invalida!"
            Lblestado.ForeColor = Color.Black

            Exit Sub
        End If

        accion = False
        If accion = False Then
            Dim fusr As Boolean = False
            Dim com As New MySqlCommand("SELECT * FROM tb_users WHERE username='" & usuario.Text & "'", connection)
            Dim datareader As MySqlDataReader

            Try
                datareader = com.ExecuteReader
                While datareader.Read()
                    If datareader(1).ToString = usuario.Text And datareader(2).ToString = md5encrypter(pass.Text) Then
                        My.Settings.username = datareader(1).ToString
                        My.Settings.displaypic = datareader(4).ToString
                        My.Settings.fname = datareader(3).ToString
                        My.Settings.Save()
                        fusr = True
                        Exit While
                    End If
                End While
                datareader.Close()
                If fusr = True Then
                    Me.Hide()
                    Dim wchat As New chatwin
                    wchat.ShowDialog()
                Else
                    Lblestado.Visible = True
                    Lblestado.ForeColor = Color.Black
                    Lblestado.Text = "Usuario o Contraseña incorrectos, pruebe de nuevo!"
                End If
            Catch
                Lblestado.Visible = True
                Lblestado.ForeColor = Color.Black
                Lblestado.Text = "Error de BD!"
            End Try
        End If
    End Sub

    Private Sub pass_Text(sender As Object, e As EventArgs) Handles pass.TextChanged

    End Sub
End Class
