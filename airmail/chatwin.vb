﻿Imports MySql.Data.MySqlClient
Imports System.Collections
Public Class chatwin

    Private Sub chatwin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = Me.Text & " - " & My.Settings.fname
        Dim mems As System.IO.MemoryStream = New System.IO.MemoryStream(Convert.FromBase64String(My.Settings.displaypic))
        foto.Image = Drawing.Image.FromStream(mems)
        usr_name.Text = My.Settings.fname
        readfriends()
        CreateEmotions()
    End Sub
    Private Sub foto_Click() Handles foto.Click
        Dim pic As New fotoperfil
        pic.ShowDialog()
    End Sub
    Private Sub readfriends()
        Dim com As New MySqlCommand("SELECT * FROM tb_users", connection)
        Dim datareader As MySqlDataReader
        datareader = com.ExecuteReader
        Dim wchat As New chatwin
        While datareader.Read()
            If Not datareader(1).ToString = My.Settings.username Then
                amigos.Items.Add(datareader(1).ToString)
            End If
        End While
        datareader.Close()
    End Sub
    Private Sub chatwin_formclosing() Handles MyBase.FormClosing
        main.Show()
    End Sub
    Private Sub timer1_Tick(sender As Object, e As EventArgs) Handles timer1.Tick
        Dim cmdreadchat As New MySqlCommand("SELECT chat FROM tb_chat", connection)
        Dim chatreader As MySqlDataReader
        chatreader = cmdreadchat.ExecuteReader
        While chatreader.Read()
            If Not chatreader(0).ToString = chatbox_r.Text Then
                chatbox_r.Text = chatreader(0).ToString
            End If
        End While
        chatreader.Close()
    End Sub
    Private Sub chatbox_textchanged() Handles chatbox.TextChanged
        chatbox.ScrollToCaret()
    End Sub
    Private Sub chatbox_textchanged(sender As Object, e As EventArgs) Handles chatbox.TextChanged
        chatbox.SelectionStart() = chatbox.TextLength
        chatbox.ScrollToCaret()
    End Sub
    Private Sub enviar_click() Handles enviar.Click
        chatbox_r.Text += vbNewLine & vbNewLine & My.Settings.username & ": " & msjbox.Text
        Dim updatechat As New MySqlCommand("UPDATE tb_chat SET chat = '" & chatbox_r.Text & "'", connection)
        updatechat.ExecuteNonQuery()
        msjbox.Text = ""
    End Sub
    Private emotions As Hashtable
    Private Sub CreateEmotions()
        emotions = New Hashtable(25)
        emotions.Add(":)", My.Resources._1)
        emotions.Add(";)", My.Resources._2)
        emotions.Add(":$", My.Resources._5)
        emotions.Add(":*", My.Resources._4)
        emotions.Add("<3", My.Resources._26)
        emotions.Add(":<3", My.Resources._3)
        emotions.Add("e.e", My.Resources._20)
        emotions.Add("-_-", My.Resources._21)
        emotions.Add(">.<", My.Resources._11)
        emotions.Add(":(angel)", My.Resources._19)
        emotions.Add(":D", My.Resources._6)
        emotions.Add("8)", My.Resources._15)
        emotions.Add(":P", My.Resources._13)
        emotions.Add("xD", My.Resources._12)
        emotions.Add(":O", My.Resources._10)
        emotions.Add("xO", My.Resources._17)
        emotions.Add(":(zzz)", My.Resources._16)
        emotions.Add("};^D", My.Resources._18)
        emotions.Add(":'(", My.Resources._7)
        emotions.Add("='(", My.Resources._8)
        emotions.Add(":(sick)", My.Resources._14)
        emotions.Add("(Y)", My.Resources._23)
        emotions.Add("(OK)", My.Resources._24)
        emotions.Add(":(|)", My.Resources._22)
        emotions.Add("(fist)", My.Resources._25)
    End Sub
    Private Sub AddEmotions()
        For Each emote As String In emotions.Keys
            While chatbox.Text.Contains(emote)
                Dim ind As Integer = chatbox.Text.IndexOf(emote)
                chatbox.Select(ind, emote.Length)
                Clipboard.SetImage(DirectCast(emotions(emote), Image))
                chatbox.Paste()
            End While
        Next
    End Sub
    Private Sub chatbox_r_TextChanged() Handles chatbox_r.TextChanged
        chatbox.Text = chatbox_r.Text
        chatbox.ReadOnly = False
        AddEmotions()
        chatbox.ReadOnly = True
    End Sub
    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Dim gal As New galeria()
        gal.ShowDialog()
        If gal.TextBox1.Text = ":)" Then
            msjbox.Text += ":)"
        End If
        If gal.TextBox1.Text = ";)" Then
            msjbox.Text += ";)"
        End If
        If gal.TextBox1.Text = ":<3" Then
            msjbox.Text += ":<3"
        End If
        If gal.TextBox1.Text = ":*" Then
            msjbox.Text += ":*"
        End If
        If gal.TextBox1.Text = ":$" Then
            msjbox.Text += ":$"
        End If
        If gal.TextBox1.Text = ":D" Then
            msjbox.Text += ":D"
        End If
        If gal.TextBox1.Text = ":'(" Then
            msjbox.Text += ":'("
        End If
        If gal.TextBox1.Text = "='(" Then
            msjbox.Text += "='("
        End If
        If gal.TextBox1.Text = ":O" Then
            msjbox.Text += ":O"
        End If
        If gal.TextBox1.Text = ">.<" Then
            msjbox.Text += ">.<"
        End If
        If gal.TextBox1.Text = "xD" Then
            msjbox.Text += "xD"
        End If
        If gal.TextBox1.Text = ":P" Then
            msjbox.Text += ":P"
        End If
        If gal.TextBox1.Text = ":(sick)" Then
            msjbox.Text += ":(sick)"
        End If
        If gal.TextBox1.Text = "8)" Then
            msjbox.Text += "8)"
        End If
        If gal.TextBox1.Text = ":(zzz)" Then
            msjbox.Text += ":(zzz)"
        End If
        If gal.TextBox1.Text = "xO" Then
            msjbox.Text += "xO"
        End If
        If gal.TextBox1.Text = "};^D" Then
            msjbox.Text += "};^D"
        End If
        If gal.TextBox1.Text = ":(angel)" Then
            msjbox.Text += ":(angel)"
        End If
        If gal.TextBox1.Text = "e.e" Then
            msjbox.Text += "e.e"
        End If
        If gal.TextBox1.Text = "-_-" Then
            msjbox.Text += "-_-"
        End If
        If gal.TextBox1.Text = ":(|)" Then
            msjbox.Text += ":(|)"
        End If
        If gal.TextBox1.Text = "(Y)" Then
            msjbox.Text += "(Y)"
        End If
        If gal.TextBox1.Text = "(fist)" Then
            msjbox.Text += "(fist)"
        End If
        If gal.TextBox1.Text = "(OK)" Then
            msjbox.Text += "(OK)"
        End If
        If gal.TextBox1.Text = "<3" Then
            msjbox.Text += "<3"
        End If
    End Sub
    Private Sub Picturebox3_formclosing() Handles MyBase.FormClosing
        Me.Hide()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Dim adj As New adjuntar()
        adj.ShowDialog()
    End Sub
    'Private Sub edprof_Click() Handles edprof.Click
    '    Dim profiedit As New editprof

    '    profiedit.ShowDialog()
    'End Sub
    'Private Sub amigos_SelectedItem() Handles amigos.Click
    '    If Not amigos.SelectedIndex = -1 Then
    '        Dim frnd As New fr
    '    End If
    'End Sub
End Class