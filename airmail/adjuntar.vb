﻿Imports System.Net.Sockets
Imports System.Threading
Public Class adjuntar
    Dim Listener1 As New TcpListener(65535)
    Dim Listener2 As New TcpListener(65534)
    Dim cliente As New UNOLibs.Net.ClientClass
    Dim WithEvents server As UNOLibs.Net.ServerClass
    Private Sub btn_conn_Click(sender As Object, e As EventArgs) Handles btn_conn.Click
        Try
            Dim pingr As String = My.Computer.Network.Ping(Me.ip.Text)
            If pingr = "True" Then
                btn_conn.Text = "Conectado"
            Else
                btn_conn.Text = "Desconectado"
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub adjuntar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckForIllegalCrossThreadCalls = False

        usuario.Text = My.Settings.fname
        usuario.Enabled = False

        server = New UNOLibs.Net.ServerClass(65533, True, ruta.Text)

        Dim ListThread As New Thread(New ThreadStart(AddressOf Listening))
        ListThread.Start()
    End Sub
    Private Sub Listening()
        Listener1.Start()
        Listener2.Start()
    End Sub
    Private Sub btn_ruta_Click(sender As Object, e As EventArgs) Handles btn_ruta.Click
        Dim buscfile As New FolderBrowserDialog()
        If buscfile.ShowDialog() = vbOK Then
            ruta.Text = buscfile.SelectedPath & "\"
            server.IncomingPath = ruta.Text
        End If
    End Sub
    Private Sub btn_enviar_Click(sender As Object, e As EventArgs) Handles btn_enviar.Click
        If buscfile.ShowDialog() = vbOK Then
            Try
                log.Text += (usuario.Text & " Esta enviando un archivo...") + vbCrLf
                cliente.SendFiles(ip.Text, 65533, buscfile.FileNames)

                With ProgressBar1
                    .Minimum = 1
                    .Maximum = 10000
                    .Value = 1
                    .Step = 1

                    For i As Integer = .Minimum To .Maximum
                        .PerformStep()
                    Next i
                End With

                If (ProgressBar1.Maximum) Then
                    MsgBox("El archivo se ha enviado con exito!", MsgBoxStyle.Information, "Notificacion")
                    ProgressBar1.Value = 1
                End If
            Catch ex As Exception

            End Try

        Else

            Exit Sub

        End If
    End Sub
    Private Sub Diagnostico(ByVal Args As String) Handles server.DiagnosticMessage
        Try
            log.Text += (usuario.Text & " - " & Args)
            With ProgressBar2
                .Minimum = 1
                .Maximum = 1000
                .Value = 1
                .Step = 1

                For i As Integer = .Minimum To .Maximum
                    .PerformStep()
                Next i
                MsgBox("La tranferencia del archivo ha finalizado correctamente!", MsgBoxStyle.Information, "Notificacion")
            End With

            'Try
            '    If (ProgressBar2.Maximum) Then
            '        MsgBox("La tranferencia del archivo ha finalizado correctamente!", MsgBoxStyle.Information, "Notificacion")
            '        ProgressBar2.Value = 1
            '    End If
            'Catch ex As Exception
            'End Try

        Catch ex As Exception

        End Try
    End Sub
End Class