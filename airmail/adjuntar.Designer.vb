﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adjuntar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_conn = New System.Windows.Forms.Button()
        Me.btn_enviar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.usuario = New System.Windows.Forms.TextBox()
        Me.ip = New System.Windows.Forms.TextBox()
        Me.ruta = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_ruta = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.log = New System.Windows.Forms.RichTextBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ProgressBar2 = New System.Windows.Forms.ProgressBar()
        Me.buscfile = New System.Windows.Forms.OpenFileDialog()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_conn
        '
        Me.btn_conn.Location = New System.Drawing.Point(335, 34)
        Me.btn_conn.Name = "btn_conn"
        Me.btn_conn.Size = New System.Drawing.Size(97, 23)
        Me.btn_conn.TabIndex = 0
        Me.btn_conn.Text = "Conectar"
        Me.btn_conn.UseVisualStyleBackColor = True
        '
        'btn_enviar
        '
        Me.btn_enviar.Location = New System.Drawing.Point(180, 65)
        Me.btn_enviar.Name = "btn_enviar"
        Me.btn_enviar.Size = New System.Drawing.Size(96, 23)
        Me.btn_enviar.TabIndex = 1
        Me.btn_enviar.Text = "Enviar archivo"
        Me.btn_enviar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Nombre de usuario:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "IP a conectar:"
        '
        'usuario
        '
        Me.usuario.Location = New System.Drawing.Point(108, 20)
        Me.usuario.Name = "usuario"
        Me.usuario.Size = New System.Drawing.Size(200, 20)
        Me.usuario.TabIndex = 4
        '
        'ip
        '
        Me.ip.Location = New System.Drawing.Point(108, 60)
        Me.ip.Name = "ip"
        Me.ip.Size = New System.Drawing.Size(200, 20)
        Me.ip.TabIndex = 5
        '
        'ruta
        '
        Me.ruta.Location = New System.Drawing.Point(106, 11)
        Me.ruta.Name = "ruta"
        Me.ruta.Size = New System.Drawing.Size(243, 20)
        Me.ruta.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ip)
        Me.Panel1.Controls.Add(Me.usuario)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btn_conn)
        Me.Panel1.Location = New System.Drawing.Point(2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(444, 98)
        Me.Panel1.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Ruta de guardado"
        '
        'btn_ruta
        '
        Me.btn_ruta.Location = New System.Drawing.Point(355, 9)
        Me.btn_ruta.Name = "btn_ruta"
        Me.btn_ruta.Size = New System.Drawing.Size(75, 23)
        Me.btn_ruta.TabIndex = 9
        Me.btn_ruta.Text = "Cambiar ruta"
        Me.btn_ruta.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btn_ruta)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.btn_enviar)
        Me.Panel2.Controls.Add(Me.ruta)
        Me.Panel2.Location = New System.Drawing.Point(4, 105)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(441, 114)
        Me.Panel2.TabIndex = 10
        '
        'log
        '
        Me.log.Location = New System.Drawing.Point(450, 2)
        Me.log.Name = "log"
        Me.log.Size = New System.Drawing.Size(266, 217)
        Me.log.TabIndex = 11
        Me.log.Text = ""
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(110, 251)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(603, 25)
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.ProgressBar1.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 251)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "enviando archivo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 304)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Recibiendo archivo"
        '
        'ProgressBar2
        '
        Me.ProgressBar2.Location = New System.Drawing.Point(110, 304)
        Me.ProgressBar2.Name = "ProgressBar2"
        Me.ProgressBar2.Size = New System.Drawing.Size(603, 25)
        Me.ProgressBar2.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.ProgressBar2.TabIndex = 15
        '
        'buscfile
        '
        Me.buscfile.FileName = "OpenFileDialog1"
        '
        'adjuntar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(717, 341)
        Me.Controls.Add(Me.ProgressBar2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.log)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "adjuntar"
        Me.Text = "adjuntar"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_conn As Button
    Friend WithEvents btn_enviar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents usuario As TextBox
    Friend WithEvents ip As TextBox
    Friend WithEvents ruta As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents btn_ruta As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents log As RichTextBox
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents ProgressBar2 As ProgressBar
    Friend WithEvents buscfile As OpenFileDialog
End Class
